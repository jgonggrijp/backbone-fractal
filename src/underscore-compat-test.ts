import * as _ from 'underscore';
import * as compat from './underscore-compat';

const lodash = !_['any'];

function requireUnderscore() {
    if (lodash) pending('This spec is only applicable to underscore.');
}

function requireLodash() {
    if (!lodash) pending('This spec is only applicable to lodash.');
}

describe('underscore-compat', function() {
    describe('reverse', function() {
        it('is a fallback function when _ is underscore', function() {
            requireUnderscore();
            expect(_['reverse']).not.toBeDefined();
            expect(compat.reverse).toEqual(jasmine.any(Function));
        });

        it('is _.reverse when _ is lodash', function() {
            requireLodash();
            expect(_['reverse']).toBeDefined();
            expect(compat.reverse).toBe(_['reverse']);
        });

        it('will reverse arrays in both cases', function() {
            let original = [1, 2, 3, 4, 5];
            let opposite = [5, 4, 3, 2, 1];
            let input = original.slice();
            let result = compat.reverse(input);
            expect(input).toEqual(opposite);
            expect(result).toBe(input);
        });
    });

    describe('eachRight', function() {
        it('is a fallback function when _ is underscore', function() {
            requireUnderscore();
            expect(_['eachRight']).not.toBeDefined();
            expect(compat.eachRight).toEqual(jasmine.any(Function));
        });

        it('is _.eachRight when _ is lodash', function() {
            requireLodash();
            expect(_['eachRight']).toBeDefined();
            expect(compat.eachRight).toBe(_['eachRight']);
        });

        it('will iterate a collection right to left in both cases', function() {
            let input = [1, 2, 3, 4, 5];
            let copy = input.slice();
            let backwards = [5, 4, 3, 2, 1];
            let sideEffect = [];
            let iterator = item => sideEffect.push(item);
            expect(compat.eachRight(input, iterator)).toBe(input);
            expect(input).toEqual(copy);
            expect(sideEffect).toEqual(backwards);
        });
    });
});
