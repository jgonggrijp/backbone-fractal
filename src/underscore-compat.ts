import { List, Dictionary, ListIterator, ObjectIterator } from 'underscore';
import * as _ from 'underscore';

namespace fallback {
    /**
     * Not chainable; use standalone only.
     */
    export function reverse<T>(array: List<T>): List<T> {
        let temp: T,
            length = array.length,
            front = 0,
            back = length - 1;
        while (front < back) {
            temp = array[front];
            array[front] = array[back];
            array[back] = temp;
            ++front, --back;
        };
        return array;
    }

    export function eachRight<T>(collection: List<T>, iteratee: ListIterator<T, void>): List<T>;
    export function eachRight<T>(collection: Dictionary<T>, iteratee: ObjectIterator<T, void>): Dictionary<T>;
    export function eachRight(collection, iteratee) {
        function wrapper(accumulator, value, key, collection) {
            iteratee(value, key, collection);
            return accumulator;
        }
        _.reduceRight(collection, wrapper, {});
        return collection;
    }
}

export interface Reverse {
    <T>(array: List<T>): List<T>;
}

export interface EachRight {
    <T>(collection: List<T>, iteratee: ListIterator<T, void>): List<T>;
    <T>(collection: Dictionary<T>, iteratee: ObjectIterator<T, void>): Dictionary<T>;
}

export const reverse: Reverse = _['reverse'] || fallback.reverse;
export const eachRight: EachRight = _['eachRight'] || fallback.eachRight;
