import _ = require('underscore');
const { src, dest, series, parallel } = require('gulp');
import browserify = require('browserify');
import vinylStream = require('vinyl-source-stream');
import vinylBuffer = require('vinyl-buffer');
import tsify = require('tsify');
import watchify = require('watchify');
import exposify = require('exposify');
import log = require('fancy-log');
import path = require('path');
import streamqueue = require('streamqueue');
import del = require('del');
import glob = require('glob');
import loadPlugins = require('gulp-load-plugins');
import yargs = require('yargs');
const plugins = loadPlugins();

type LibraryProps = {
    module: string,
    browser?: string,
    global: string,
    alias?: string[],
    path?: string,
    package?: string,
};
type ExposeConfig = {
    [moduleName: string]: string,
};

// General configuration.
const sourceDir = `src`,
    buildDir = `dist`,
    bundleDir = '.',
    testDir = 'test',
    nodeDir = `node_modules`,
    mainScript = `${sourceDir}/index.ts`,
    jsBundleName = `backbone-fractal.js`,
    jsBundleDest = `${bundleDir}/${jsBundleName}`,
    jsSourceMapDest = `${bundleDir}/${jsBundleName}.map`,
    jsModuleType = `commonjs`,
    tsOptions = {
        target: `es5`,
        lib: ['DOM', 'ES5', 'ES6', 'DOM.Iterable', 'ScriptHost'],
        // baseUrl: '.',
    },
    unittestBundleName = 'tests.js',
    unittestEntries = glob.sync(`${sourceDir}/**/*-test.ts`),
    indexOutput = 'specRunner.html',
    lodash = yargs.argv.lodash,
    underscoreImplementation = lodash && 'lodash' || 'underscore',
    underscoreAlternative = lodash && 'underscore' || 'lodash';

// Libraries which are inserted through <script> tags rather than being bundled
// by Browserify. They will be inserted in the order shown.
const browserLibs: LibraryProps[] = [{
        module: 'jquery',
        global: '$',
    }, {
        module: underscoreImplementation,
        global: '_',
        alias: [underscoreAlternative],
    }, {
        module: 'backbone',
        global: 'Backbone',
    }],
    browserLibsRootedPaths: string[] = [];

browserLibs.forEach(lib => {
    let browser = lib.browser || lib.module;
    lib.path = path.relative(nodeDir, require.resolve(browser));
    browserLibsRootedPaths.push(path.join(nodeDir, lib.path));
});

// We override the filePattern (normally /\.js$/) because tsify
// outputs files without an extension. Basically, we tell exposify to
// not be picky. This is fine because we only feed JS files into
// browserify anyway.
exposify.filePattern = /./;
exposify.config = browserLibs.reduce((config: ExposeConfig, lib) => {
    config[lib.module] = lib.global;
    if (lib.alias) lib.alias.forEach(alias => config[alias] = lib.global);
    return config;
}, {});

function decoratedBrowserify(options) {
    return browserify(options)
        .plugin(tsify, tsOptions)
        .transform(exposify, {global: true});
}

const tsModules = decoratedBrowserify({
    debug: true,
    standalone: 'BackboneFractal',
    entries: [mainScript],
});

const tsTestModules = decoratedBrowserify({
    debug: true,
    entries: unittestEntries,
    cache: {},
    packageCache: {},
});

export function bundle() {
    return tsModules.bundle()
        .pipe(vinylStream(jsBundleName))
        .pipe(vinylBuffer())
        .pipe(plugins.sourcemaps.init({loadMaps: true}))
        .pipe(plugins.uglify())
        .pipe(plugins.sourcemaps.write(bundleDir))
        .pipe(dest(bundleDir));
}

function jsUnittest() {
    const libs = src(browserLibsRootedPaths);
    const bundle = tsTestModules.bundle()
        .pipe(vinylStream(unittestBundleName))
        .pipe(vinylBuffer());
    return streamqueue({objectMode: true}, libs, bundle)
        .pipe(plugins.jasmineBrowser.specRunner({console: true}))
        .pipe(dest(testDir))
        .pipe(plugins.connect.reload());
}

export function compile() {
    return src([`${sourceDir}/**/*.ts`, `!${sourceDir}/**/*-test.ts`])
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.typescript(_.defaults({
            declaration: true,
            module: 'umd',
        }), tsOptions))
        .pipe(plugins.sourcemaps.write('.', {
            sourceRoot: `../${sourceDir}`,
            includeContent: false,
        }))
        .pipe(dest(buildDir));
}

export function test() {
    const headless = plugins.jasmineBrowser.headless({driver: 'phantomjs'});
    // The next line is a fix based on
    // https://github.com/gulpjs/gulp/issues/71#issuecomment-41512070
    headless.on('error', e => headless.end());
    return jsUnittest().pipe(headless);
}

export function serve() {
    let serverOptions: any = {
        root: testDir,
        livereload: true,
        fallback: indexOutput,
    };
    plugins.connect.server(serverOptions);
}

export function watch() {
    tsTestModules.plugin(watchify);
    tsTestModules.on('update', jsUnittest);
    tsTestModules.on('log', log);
    jsUnittest();
}

export function clean() {
    return del([buildDir, testDir, jsBundleDest, jsSourceMapDest]);
}

export const dist = series(clean, parallel(compile, bundle));

export default series(clean, parallel(watch, serve));
